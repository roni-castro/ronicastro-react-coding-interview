import { useContactEdit } from '@hooks/contacts/useContactEdit';
import { useParams } from 'react-router-dom';

export const ContactPersonDetails = () => {
  const { id } = useParams();
  const { isLoading, isError, contact } = useContactEdit(id);

  if (isLoading) return <div>Loading...</div>;
  if (isError) return <div>Error loading data</div>;

  return (
    <div>
      <p>Full Name</p>
      <p>Email</p>
      <p>{contact.email}</p>
    </div>
  );
};

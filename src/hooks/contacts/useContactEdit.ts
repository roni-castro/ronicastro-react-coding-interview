import { useCallback, useEffect, useState } from 'react';
import { contactsClient } from '@lib/clients/contacts';
import { IPerson } from '@lib/models/person';

export interface IContactDetails {
  status: 'idle' | 'loading' | 'success' | 'error';
  contact: IPerson | null;
}

const initialState: IContactDetails = {
  status: 'idle',
  contact: null
};

const fetchContactDetailsById = (id: string) => contactsClient.getContactById(id);

export function useContactEdit(id: string) {
  const [contactResult, setContactResult] = useState<IContactDetails>(initialState);

  const isLoading = contactResult.status === 'idle' || contactResult.status === 'loading';
  const isError = contactResult.status === 'error';

  const fetchContactDetails = useCallback(async (id: string) => {
    try {
      setContactResult(c => ({ ...c, status: 'loading' }));
      const res = await fetchContactDetailsById(id);
      setContactResult({
        contact: res,
        status: 'success'
      });
    } catch (e) {
      setContactResult(c => ({
        ...c,
        status: 'error'
      }));
    }
  }, []);

  useEffect(() => {
    fetchContactDetails(id);
  }, [fetchContactDetails, id]);

  return {
    contact: contactResult.contact,
    isLoading,
    isError,
    update: useCallback((updated: IPerson) => {
      console.log(updated);
      throw new Error('Not implemented!');
    }, [])
  };
}
